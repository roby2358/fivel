package yuwakisa.fivel.models.words

import scala.collection.mutable
import scala.util.Random

object Associator :
  val Start = "<"
  val End = ">"

  /**
   * Clean the < > from the string
   */
  def clean(s: String): String =
    s.replace(Start,"").replace(End,"")

class Associator(maxN: Int, val words: Seq[String])(implicit random: Random) :

  val links: mutable.Map[String, mutable.Map[String, Int]] =
    mutable.Map().withDefault(_ => mutable.Map().withDefault(_ => 0))

  /**
   * split the words up into pairs ngrams
   */
  def ngramPairs(word: String): Seq[(String, String)] =
    // add a terminator so we know when to stop
    val w = Associator.Start + word + Associator.End

    for (i <- 1 to math.min(maxN, w.length - 1);
          j <- 1 to math.min(maxN, w.length - i);
          ii <- 0 to w.length - i - j
         ) yield
            val a = w.substring(ii, ii + i)
            val b = w.substring(ii + i, ii + i + j)
            (a, b)

  def incr(n: Option[Int]): Option[Int] = n.map(_ + 1).orElse(Some(1))

  /**
   * Run through the ngram pairs to increment each link
   * @return this
   */
  def build(): Associator =
    words.foreach { w =>
      ngramPairs(w).foreach {
        case (a, b) =>
          // have to update a in order to override the default :shrug:
          links.update(a, links(a))
          links(a).updateWith(b)(incr)
      }
    }
    this

  def choices(w: String): mutable.Map[String, Int] =
    val choices: mutable.Map[String, Int] = mutable.Map().withDefault(_ => 0)
    for (
      i <- 1 to math.min(maxN, w.length);
      cc <- links(w.substring(w.length - i)).toSeq)
        choices.updateWith(cc._1) { n => n.map(_ + cc._2).orElse(Option(cc._2)) }
    choices

  def choose(choices: Seq[(String, Int)])(implicit random: Random) : String =
    val total = choices.map(_._2).sum
    val n = random.nextInt(total)
    choices.foldLeft(("", n)) {
      case ((best, r), (s, m)) if r >= 0 => (s, r - m)
      case ((best, r), (s, m)) => (best, r)
    }._1

  /**
   * @return a generated string
   */
  def roll(): String =
    var w = Associator.Start
    var cc: mutable.Map[String, Int] = mutable.Map()
    while
      cc = choices(w)
      cc.nonEmpty
    do
      w += choose(cc.toSeq)
    Associator.clean(w)

  /**
   * Keeps rolling until we get one that's not already in the list
   * @return a unique new word
   */
  def uniq(): String =
    Stream.continually({
      roll() match
        case w if words.contains(w) => None
        case w => Option(w)
    })
      .find(_.isDefined)
      .get
      .get
