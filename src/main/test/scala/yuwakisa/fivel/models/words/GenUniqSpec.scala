package yuwakisa.fivel.models.words

import org.junit.Assert.{assertEquals, assertTrue}
import org.junit.Test

import scala.collection.mutable
import scala.util.Random

import yuwakisa.fivel.models.basic.Resource

class GenUniqSpec :

  implicit val random: Random = Random()

  def counter: mutable.Map[String, Int] = new scala.collection.mutable.HashMap[String, Int]().withDefaultValue(0)

  @Test def rollUniq(): Unit =
    val a = Associator(3, Seq("abcd", "abcx", "abcy", "abcz")).build()
    val c = counter
    (0 until 4000).foreach { _ =>
      c(a.roll()) += 1
    }
    println(c)

  @Test def bondVillains(): Unit =
    val names = Resource("/words/bondvillains.txt").toSeq
    println(names)
    val a = Associator(5, names).build()
    for (i <- 0 until 20)
      println(a.roll())

/*
Horor Linic
Kongo Elede
Lee Frifex
Maxwiton Monro
Sereves
Fran Lior Dextex
Jeantin Hugmun
Hecto Safin
Sig Weisen
Mariguez
Gluggsy Maringo
Espadafina Severa
Volon von Budam
Goldovan Gol Kobudevicher
Nenasco Deng
*/

  @Test def english(): Unit =
    val names = Resource("/words/english.txt").toSeq
    val a = Associator(5, names).build()
    for (i <- 0 until 20)
      println(a.uniq())

/*
wacteriention
compty
sententist
convimpt
soluce
appainfuago
menunonoontinsise
inglishat
vilie
opretal
resebration
mobal
indmire
*/