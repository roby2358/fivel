package yuwakisa.fivel.models.texts

import yuwakisa.fivel.models.basic.Resource

object TaoTeChing:
  def main(args: Array[String]): Unit =
    val r = new Resource("/text/taoteching.txt").toSeq.map { m =>
      m.trim
      .replaceAll("Chapter [0-9]+", "")
      .replaceAll("Part [ivxlcmIVXLCM]+", "")
      .replaceAll("""---+""", "")
      .replaceAll("""[0-9]\. """, " ")
      .replaceAll("[;:]|--", " , ")
      .replaceAll("[:;] ?--", " , ")
      .replaceAll("[()\"']", "")
      .replaceAll("([.,?!])", " $1 ")
      .replace("\n", " ")
    }
      .filter(_.nonEmpty)
    println(r.mkString(" ")
      .replaceAll("  +", " ")
      .grouped(72).mkString("\n"))
