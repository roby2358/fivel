package yuwakisa.fivel.models.tables

import org.junit.Assert.{assertEquals, assertNull, assertTrue}
import org.junit.Test

import java.io._

class EvalSpec :

  object Ev extends Eval with TestEvalinator

  @Test def plain: Unit =
    val got = Ev.eval("a,aa")
    assertEquals("a,aa", got)

  @Test def evaluated: Unit =
    val got = Ev.eval("{b-bb}")
    assertEquals("B-BB", got)

  @Test def mix1: Unit =
    val got = Ev.eval("a,aa{b-bb}")
    assertEquals("a,aaB-BB", got)

  @Test def mix2: Unit =
    val got = Ev.eval("{b-bb}c,cc")
    assertEquals("B-BBc,cc", got)

  @Test def mix3: Unit =
    val got = Ev.eval("a,aa{b-bb}c*cc{d=dd}e&ee")
    assertEquals("a,aaB-BBc*ccD=DDe&ee", got)

  @Test def dice0: Unit =
    val got = Ev.eval("[]")
    assertEquals("1006", got)

  @Test def dice1: Unit =
    val got = Ev.eval("[2d5]")
    assertEquals("2005", got)

  @Test def dice2: Unit =
    val got = Ev.eval("[d7]")
    assertEquals("1007", got)

  @Test def dice3: Unit =
    val got = Ev.eval("[3d]")
    assertEquals("3006", got)

  @Test def dice4: Unit =
    val got = Ev.eval("[d]")
    assertEquals("1006", got)
