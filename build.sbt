ThisBuild / scalaVersion := "3.1.1"

lazy val root = project
  .in(file("."))
  .settings(
    name := "fivel",
    organization := "org.yuwakisa",
    version := "1.0-SNAPSHOT",

    scalacOptions ++= Seq(
      "-language:implicitConversions",
//      "-old-syntax",
//      "-new-syntax",
      "-indent",
      "-Yindent-colons"
      //      "-rewrite"
    ),

    run / run / mainClass := Some("yuwakisa.fivel.Main"),

    resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",

    libraryDependencies ++= Seq(
      "com.h2database" % "h2" % "1.4.200",
      "org.eclipse.jetty" % "jetty-servlet" % "9.3.12.v20160915",
      "org.eclipse.jetty" % "jetty-server" % "9.3.12.v20160915",
      "commons-io" % "commons-io" % "2.11.0",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.1",
      "org.scalatest" %% "scalatest" % "3.2.11" % Test,
      "junit" % "junit" % "4.13.2" % Test,
    ),
)