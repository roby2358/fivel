package yuwakisa.fivel.models.words

import org.junit.Assert.{assertEquals, assertTrue}
import org.junit.Test

import scala.util.Random

import yuwakisa.fivel.models.basic.Resource

class WordsSpec :

  implicit val random: Random = Random(1234567890)

  @Test def name(): Unit =
    val actual = Words.name("/blah/blah/blah/zing.txt")
    val expected = "zing"
    assertEquals(expected, actual)

  @Test def namePlain(): Unit =
    val actual = Words.name("zoop")
    val expected = "zoop"
    assertEquals(expected, actual)

  @Test def parseOne(): Unit =
    val (n, a) = Words.parseOne("/resources/resources/a.txt")
    assertEquals("a", n)
    assertEquals("abx", a.words.head)
    assertEquals("bdz", a.build().roll())

  @Test def load(): Unit =
    val r = Resource("/resources/resources")
    val actual = Words.load(r.paths)
    assertEquals(Seq("a", "b", "c"), actual.toSeq)
    assertEquals(Right("aced"), actual.uniq("b"))
