package yuwakisa.fivel.models.words

import org.junit.Assert.{assertEquals, assertTrue}
import org.junit.Test

import scala.collection.mutable
import scala.util.Random

class AssociatorSpec :

  implicit val random: Random = Random(1234)

  @Test def ngramPairsLong: Unit =
    val actual = Associator(3, null).ngramPairs("bcde")
    val expected = Seq(
      ("<","b"), ("b","c"), ("c","d"), ("d","e"), ("e",">"),
      ("<","bc"), ("b","cd"), ("c","de"), ("d","e>"),
      ("<","bcd"), ("b","cde"), ("c","de>"),
      ("<b","c"), ("bc","d"), ("cd","e"), ("de",">"),
      ("<b","cd"), ("bc","de"), ("cd","e>"),
      ("<b","cde"), ("bc","de>"),
      ("<bc","d"), ("bcd","e"), ("cde",">"),
      ("<bc","de"), ("bcd","e>"),
      ("<bc","de>"))
    assertEquals(expected, actual)

  @Test def ngramPairs2: Unit =
    val actual = Associator(3, null).ngramPairs("b")
    val expected = Seq(
      ("<","b"), ("b",">"),
      ("<","b>"),
      ("<b",">"))
    assertEquals(expected, actual)

  @Test def ngramPairs1: Unit =
    val actual = Associator(3, null).ngramPairs("")
    val expected = Seq(("<",">"))
    assertEquals(expected, actual)

  @Test def build2: Unit =
    val actual = Associator(3, Seq("")).build().links
    val expected = mutable.Map("<" -> mutable.Map(">" -> 1))
    assertEquals(expected, actual)

  @Test def build3: Unit =
    val actual = Associator(3, Seq("b")).build().links
    val expected = mutable.Map(
      "<" -> mutable.Map("b" -> 1, "b>" -> 1),
      "<b" -> mutable.Map(">" -> 1),
      "b" -> mutable.Map(">" -> 1))
    assertEquals(expected, actual)

  @Test def linksStart: Unit =
    val actual = Associator(3, Seq("a")).build().links("<").toSet
    val expected = Set(("a",1), ("a>",1))
    assertEquals(expected, actual)

  @Test def linksLast: Unit =
    val actual = Associator(3, Seq("abc")).build().links("c").toSet
    val expected = Set((">",1))
    assertEquals(expected, actual)

  @Test def linksMiddle: Unit =
    val actual = Associator(3, Seq("abc")).build().links("a").toSet
    val expected = Set(("b",1), ("bc",1), ("bc>",1))
    assertEquals(expected, actual)

  @Test def linksEnd: Unit =
    val actual = Associator(3, Seq("abc")).build().links(">").toSet
    val expected = Set()
    assertEquals(expected, actual)

  @Test def choices1: Unit =
    val actual = Associator(3, Seq("b")).build().choices("<")
    val expected = mutable.Map("b" -> 1, "b>" -> 1)
    assertEquals(expected, actual)

  @Test def choices3: Unit =
    val actual = Associator(3, Seq("abc")).build().choices("a")
    val expected = mutable.Map("bc" -> 1, "b" -> 1, "bc>" -> 1)
    assertEquals(expected, actual)

  @Test def choose1: Unit =
    val actual = Associator(3, Seq("")).choose(Seq(("a", 1)))
    val expected = "a"
    assertEquals(expected, actual)

  @Test def choose2: Unit =
    val actual = Associator(3, Seq("")).choose(Seq(("a", 1), ("b", 2), ("c", 3)))
    val expected = "b"
    assertEquals(expected, actual)

  @Test def choose3: Unit =
    val actual = Associator(3, Seq("")).choose(Seq(("a", 2), ("b", 2)))
    val expected = "b"
    assertEquals(expected, actual)

  @Test def choose4: Unit =
    val actual = Associator(3, Seq("")).choose(Seq(("a", 3), ("b", 2)))
    val expected = "b"
    assertEquals(expected, actual)

  @Test def gen1: Unit =
    val actual = Associator(3, Seq("b")).build().roll()
    val expected = "b"
    assertEquals(expected, actual)

  @Test def gen2: Unit =
    val actual = Associator(3, Seq("abcde")).build().roll()
    val expected = "abcde"
    assertEquals(expected, actual)

  @Test def gen3: Unit =
    val actual = Associator(3, Seq("abcx", "abcx", "abcy", "abcz")).build().roll()
    val expected = "abcx"
    assertEquals(expected, actual)

  @Test def uniq1: Unit =
    val actual = Associator(3, Seq("abcx", "abcx", "abcy", "abcz", "abbc", "accb")).build().uniq()
    val expected = "abccb"
    assertEquals(expected, actual)
