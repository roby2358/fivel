package yuwakisa.fivel

import yuwakisa.servel.{HelloWorldServlet, ServerRunner, StaticContentServlet}

object Main:
  val routes = Map( "/" -> classOf[StaticContentServlet],
    "/hello" -> classOf[HelloWorldServlet],
    "/tables/*" -> classOf[TablesServlet],
    "/words/*" -> classOf[WordsServlet],
    "/texts/*" -> classOf[TextsServlet])

  val runner = new ServerRunner(routes)

  def main(args: Array[String]): Unit =
    runner.start()
