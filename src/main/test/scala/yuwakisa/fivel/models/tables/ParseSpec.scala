package yuwakisa.fivel.models.tables

import yuwakisa.fivel.models.tables.Parse.State
import yuwakisa.fivel.models.tables._
import org.junit.Assert.{assertEquals, assertNull, assertTrue}
import org.junit.Test

import java.io._
import scala.collection.mutable

class ParseSpec :

  def given_input(s: String): (BufferedReader, Parse) =
    val r = BufferedReader(StringReader(s))
    (r, Parse(r))

  @Test def yesComment(): Unit =
    // for now, there has to be a newline or it loops infinitely :\
    val input = "# comment\n"
    val (r, p) = given_input(input)
    p.go
    assertEquals(State.StartLine, p.state)
    assertTrue(p.lex.done)
    assertNull(r.readLine)

  @Test def oneTable(): Unit =
    val input = "o,neT-able"
    val expected = """= o,neT-able
      |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertNull(r.readLine)
    assertEquals(expected, p.mkString)
    assertEquals(1, p.tables.length)
    assertEquals(Table("o,neT-able"), p.tables(0))

  @Test def twoTables(): Unit =
    val input = "t,woT-ables\nb, t-able"
    val expected = """= t,woT-ables
                     |= b, t-able
                     |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)
    assertEquals(2, p.tables.length)
    assertNull(r.readLine)

  @Test def missingTable(): Unit =
    val input = "  1 row\n"
    val (r, p) = given_input(input)
    p.go
    assertTrue(p.tables.isEmpty)

  @Test def oneRow(): Unit =
    val input = "oneRow\n 10 row"
    val expected = """= oneRow
                     |-- 10 row
                     |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)
    assertEquals(1, p.tables.length)
    assertEquals(1, p.tables(0).rows.length)
    assertNull(r.readLine)

    val table0 = p.tables(0)
    assertEquals("oneRow", table0.name)

    assertEquals(1, table0.rows.length)
    val row0 = table0.rows(0)
    assertEquals(1, row0.indent)
    assertEquals(10, row0.freq)
    assertEquals("row", row0.string)

  @Test def oneRowNoCount(): Unit =
    val input = "singleRow\n first"
    val expected = """= singleRow
                     |-- 1 first
                     |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)
    assertEquals(1, p.tables.length)
    assertEquals(1, p.tables(0).rows.length)
    assertNull(r.readLine)

    val table0 = p.tables(0)
    assertEquals("singleRow", table0.name)

    assertEquals(1, table0.rows.length)
    val row0 = table0.rows(0)
    assertEquals(1, row0.indent)
    assertEquals(1, row0.freq)
    assertEquals("first", row0.string)

  @Test def twoRows(): Unit =
    val input = "twoRows\n 1 row\n 2 wor"
    val expected = """= twoRows
                     |-- 1 row
                     |-- 2 wor
                     |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)
    println(p.tables)
    assertEquals(1, p.tables.length)
    assertEquals(2, p.tables(0).rows.length)
    assertNull(r.readLine)

  @Test def twoTablesWitRows(): Unit =
    val input = "twoTablesWitRows\n 1 baluga\nother table\n   1 some row"
    val expected = """= twoTablesWitRows
                     |-- 1 baluga
                     |= other table
                     |-- 1 some row
                     |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)
    assertEquals(2, p.tables.length)
    assertEquals(1, p.tables(0).rows.length)
    assertEquals(1, p.tables(1).rows.length)
    assertNull(r.readLine)

  @Test def nestedRows(): Unit =
    val input = "nestedRows\n-2 wombat\n----3 tiger\n----4 duck\n-5 zebra"
    val expected = """= nestedRows
                     |-- 2 wombat
                     |---- 3 tiger
                     |---- 4 duck
                     |-- 5 zebra
                     |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)

    assertEquals(1, p.tables.length)

    val table0 = p.tables(0)
    assertEquals("nestedRows", table0.name)

    assertEquals(2, table0.rows.length)
    val row0 = table0.rows(0)
    assertEquals(1, row0.indent)
    assertEquals(2, row0.freq)
    assertEquals("wombat", row0.string)

    assertEquals(2, row0.rows.length)

    assertEquals(4, row0.rows(0).indent)
    assertEquals(3, row0.rows(0).freq)
    assertEquals("tiger", row0.rows(0).string)

    assertEquals(4, row0.rows(1).indent)
    assertEquals(4, row0.rows(1).freq)
    assertEquals("duck", row0.rows(1).string)

    val row1 = table0.rows(1)
    assertEquals(1, row1.indent)
    assertEquals(5, row1.freq)
    assertEquals("wombat", row0.string)

    assertNull(r.readLine)

  @Test def nestedRows2(): Unit =
    val input = "nestedRows2\n 2 aaa\n    3 bbb\n  4 ccc"
    val expected = """= nestedRows2
                     |-- 2 aaa
                     |---- 3 bbb
                     |---- 4 ccc
                     |""".stripMargin.replaceAll("[\r]","")
    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)

  @Test def yesParse(): Unit =
    val input = """

# some stuff
# more stuff
bings
  1   bing
    1    bang
      # more
      1 zing
----1 bling

booms
  # woo
--1 boom
  # done
"""
    val expected = """= bings
                     |-- 1 bing
                     |---- 1 bang
                     |------ 1 zing
                     |---- 1 bling
                     |= booms
                     |-- 1 boom
                     |""".stripMargin.replaceAll("[\r]","")

    val (r, p) = given_input(input)
    p.go
    assertEquals(expected, p.mkString)
    assertNull(r.readLine)

  @Test def roundTrip(): Unit =
    val input = """= roundTrip
               |-- 1234567890 bingo bongo
               |""".stripMargin.replaceAll("[\r]","")
    val (r0, p0) = given_input(input)
    p0.go
    val (r1, p1) = given_input(p0.mkString)
    p1.go
    assertEquals(input, p1.mkString)
    assertNull(r1.readLine)
