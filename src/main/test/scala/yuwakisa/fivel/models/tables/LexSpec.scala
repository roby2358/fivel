package yuwakisa.fivel.models.tables

import org.junit.Assert.{assertEquals, assertTrue}
import org.junit.Test

import java.io._
import java.nio.charset.StandardCharsets
import scala.collection.mutable

class LexSpec :

  def given_input(s: String): BufferedReader =
    BufferedReader(StringReader(s))

  // indent

  @Test def noIndent: Unit =
    val r = given_input("x    y")
    val in = Lex(r).indent
    assertEquals(Some(0), in)
    assertEquals("x    y", r.readLine)

  @Test def indent: Unit =
    val r = given_input("    xy")
    val in = Lex(r).indent
    assertEquals(Some(4), in)
    assertEquals("xy", r.readLine)

  @Test def eolIndent: Unit =
    val r = given_input("")
    val in = Lex(r).indent
    assertEquals(Some(0), in)
    assertEquals(null, r.readLine)

  // number

  @Test def noNumber: Unit =
    val r = given_input("abc")
    val in = Lex(r).number
    assertEquals(None, in)
    assertEquals("abc", r.readLine)

  @Test def yesNumber: Unit =
    val r = given_input("123abc")
    val in = Lex(r).number
    assertEquals(Some(123), in)
    assertEquals("abc", r.readLine)

  @Test def yesEolNumber: Unit =
    val r = given_input("123")
    val in = Lex(r).number
    assertEquals(Some(123), in)
    assertEquals(null, r.readLine)

  @Test def eolNumber: Unit =
    val r = given_input("")
    val in = Lex(r).number
    assertEquals(None, in)
    assertEquals(null, r.readLine)

  // Table Start
  @Test def noTableStart: Unit =
    val r = given_input("blah")
    val in = Lex(r).tableStart
    assertEquals(false, in)
    assertEquals("blah", r.readLine)

  @Test def yesTableStart: Unit =
    val r = given_input("= blah")
    val in = Lex(r).tableStart
    assertEquals(true, in)
    assertEquals(" blah", r.readLine)

  // String

  @Test def yesString: Unit =
    val r = given_input("a, s-trin&g\nb, s-trin&g")
    val in = Lex(r).string
    assertEquals(Some("a, s-trin&g"), in)
    assertEquals("b, s-trin&g", r.readLine)

  @Test def yesFunnyCharString: Unit =
    val r = given_input("a, s\u2019trin&g\nb, s-trin&g")
    val in = Lex(r).string
    assertEquals(Some("a, s"), in)
    assertEquals("trin&g", r.readLine)

  @Test def yesEolString: Unit =
    val r = given_input("a, s-trin&g")
    val in = Lex(r).string
    assertEquals(Some("a, s-trin&g"), in)
    assertEquals(null, r.readLine)

  @Test def noString: Unit =
    val r = given_input("")
    val in = Lex(r).string
    assertEquals(None, in)
    assertEquals(null, r.readLine)

  // Comment

  @Test def noComment: Unit =
    val r = given_input("no comment")
    val in = Lex(r).comment
    assertEquals(false, in)
    assertEquals("no comment", r.readLine)

  @Test def yesOneComment: Unit =
    val r = given_input("# comment")
    val lex = Lex(r)
    val in = lex.comment
    assertEquals(true, in)
    assertEquals(null, r.readLine)
    assertTrue(lex.done)

  @Test def yesComment: Unit =
    val r = given_input("\n\n\n\n# comment\nwoo")
    val in = Lex(r).comment
    assertEquals(true, in)
    assertEquals("woo", r.readLine)

  @Test def newlineComment: Unit =
    val r = given_input("\n\n\n\n\nwoo")
    val in = Lex(r).comment
    assertEquals(false, in)
    assertEquals("woo", r.readLine)

  @Test def onlyComment: Unit =
    val r = given_input("# comment\nwoo")
    val in = Lex(r).comment
    assertEquals(true, in)
    assertEquals("woo", r.readLine)

  @Test def eolComment: Unit =
    val r = given_input("")
    val in = Lex(r).comment
    assertEquals(false, in)
    assertEquals(null, r.readLine)

  // done
  @Test def noDone: Unit =
    val r = given_input("a")
    val in = Lex(r).done
    assertEquals(false, in)
    assertEquals("a", r.readLine)

  @Test def yesDone: Unit =
    val r = given_input("")
    val in = Lex(r).done
    assertEquals(true, in)
    assertEquals(null, r.readLine)
