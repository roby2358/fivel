package yuwakisa.fivel.models.basic

import org.junit.Assert.{assertEquals, assertTrue}
import org.junit.Test
import yuwakisa.fivel.models.basic.Resource

class ResourceSpec  :

  @Test def directiryReader(): Unit =
    val actual = Resource("/resources/testtables").reader.get.readLine
    val expected = "info.txt"
    assertEquals(expected, actual)

  @Test def readerFile(): Unit =
    val actual = Resource("/resources/testtables/info.txt").reader.get.readLine
    val expected = "= Info"
    assertEquals(expected, actual)

  @Test def directoryToSeq(): Unit =
    val actual = Resource("/resources/testtables").toSeq
    val expected = Seq("info.txt")
    assertEquals(expected, actual)

  @Test def content(): Unit =
    val r = Resource("/resources/testtables/info.txt")
    val actual = r.reader.map(r.readAll)
    val expected = Some(List("= Info", "  1 Facts", "  1 History", "  1 Math", "  1 Stories", "  1 Banannas"))
    assertEquals(expected, actual)

  @Test def paths(): Unit =
    val actual = Resource("/resources/resources").paths
    val expected = Seq("/resources/resources/a.txt", "/resources/resources/b.txt", "/resources/resources/c.txt")
    assertEquals(expected, actual)

  @Test def pathsTrailingSlash(): Unit =
    val actual = Resource("/resources/resources/").paths
    val expected = Seq("/resources/resources/a.txt", "/resources/resources/b.txt", "/resources/resources/c.txt")
    assertEquals(expected, actual)
