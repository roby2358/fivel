import os
import re
import requests
import concurrent.futures

STORY_DIR = 'stories'

BASE_URL = 'https://www.bodyinflation.org'
STORY_URL_PATTERN = re.compile(r'<a href="(/node/\d+)">')
NEXT_URL_PATTERN = re.compile(r'<li class="pager-next"><a href=".*page=(\d+)')

def scrape_page(url):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        while True:
            response = requests.get(url)
            html_content = response.text

            story_urls = STORY_URL_PATTERN.findall(html_content)

            futures = []
            for story_url in story_urls:
                futures.append(executor.submit(scrape_story, story_url))

            concurrent.futures.wait(futures)

            next_url_match = NEXT_URL_PATTERN.search(html_content)
            if not next_url_match:
                break

            url = BASE_URL + '/Library?page=' + next_url_match.group(1)

            print(url)

replacements = [
    ("&nbsp;", ""),
    ("<p>", ""),
    ("</p>", ""),
    ("\u2018", "'"),
    ("\u2019", "'"),
    ("\u201C", "\""),
    ("\u201D", "\""),
    ("\u2013", "-"),
    ("\u2014", "-"),
    ("\u2026", "..."),
    ("\u00A0", " "),
    ("\u2022", "*"),
]

TITLE_PATTERN = re.compile(r'<title>([^|]+)')
STORY_PATTERN = re.compile(r'(<p>.*?)<div', re.DOTALL)
ALPHANUM = re.compile(r'[^a-zA-Z0-9]+')

def scrape_story(story_url):
    response = requests.get(BASE_URL + story_url)
    content = response.content.decode('utf-8')

    title = TITLE_PATTERN.findall(content)[0]
    title = ALPHANUM.sub('_', title.strip())

    print(title)

    story = STORY_PATTERN.findall(content)[0]
    for old, new in replacements:
        story = story.replace(old, new)
    story = ''.join([i if ord(i) < 128 else '' for i in story])

    with open(os.path.join(STORY_DIR, title + '.txt'), 'w') as f:
        f.write(story)

if __name__ == '__main__':
    if not os.path.exists(STORY_DIR):
        os.makedirs(STORY_DIR)

    scrape_page(BASE_URL + '/library')