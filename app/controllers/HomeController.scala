package controllers

import anorm.SqlParser._
import anorm._
import controllers.HomeController.getClass
import models.basic.Resource
import models.tables.{Parse, Tables}
import models.words._
import play.api.Environment
import play.api.db.Database
import play.api.libs.json._
import play.api.mvc._

import java.io.{BufferedReader, InputStreamReader}
import java.sql.Connection
import javax.inject._
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.util.Random

object HomeController :

  implicit val random: Random = Random()

/**
   * The list of table files to read
   */
  lazy val TableSources = Resource("/tables/").paths
  lazy val AllTables = Tables.load(TableSources)
  lazy val AllTableNames = AllTables.toSeq

  /**
   * The list of word generators to read
   */
  lazy val WordSources = Resource("/words/").paths
  lazy val AllWords = Words.load(WordSources)
  lazy val AllWordsNames = AllWords.toSeq

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(
                                val controllerComponents: ControllerComponents
                              )extends BaseController :

  import HomeController._

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      Ok(views.html.index("FiveL"))
    }

  /**
   * presents the list of tables as JSON
   */
  def tables(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      Ok(Json.toJson(Map("tables" -> AllTableNames)))
    }

  /**
   * roll a single table
   */
  def rollTable(name: String): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      AllTables(name)(Tables.mkString) match
        case Left(error) =>
          BadRequest(Json.toJson(Map("error" -> error)))
        case Right(string) =>
          println(string)
          val result = Map("rolled" -> string)
          Ok(Json.toJson(result))
    }

  /**
   * presents the list of word generators as JSON
   */
  def words(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      Ok(Json.toJson(Map("words" -> AllWordsNames)))
    }

  /**
   * roll a single word
   */
  def rollWord(name: String): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      AllWords(name) match
        case Left(error) =>
          BadRequest(Json.toJson(Map("rolled" -> error)))
        case Right(string) =>
          val result = Map("rolled" -> string)
          Ok(Json.toJson(result))
    }
