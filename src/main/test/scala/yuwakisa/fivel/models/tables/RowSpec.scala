package yuwakisa.fivel.models.tables

import org.junit.Assert.{assertEquals, assertNull, assertTrue}
import org.junit.Test

import java.io._

class RowSpec :
  def given_input(s: String): Parse =
    val r = BufferedReader(StringReader(s))
    Parse(r).go

  @Test def oneCumulaive(): Unit =
    val in = """= Dist
        |  10 o,ne
        |""".stripMargin
      val t = given_input(in)
      assertEquals(Seq(10), t.tables.head.cumulative)

  @Test def threeCumulaive(): Unit =
    val in = """= Dist
               |  10 o,ne
               |    2 o-ne
               |    4 o&ne
               |  20 o=ne
               |  30 o!ne
               |""".stripMargin
    val t = given_input(in)
    assertEquals(Seq(10, 30, 60), t.tables.head.cumulative)
