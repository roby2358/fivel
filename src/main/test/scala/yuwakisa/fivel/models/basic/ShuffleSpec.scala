package yuwakisa.fivel.models.basic

import org.junit.Assert.{assertEquals, assertTrue}
import org.junit.Test

import scala.collection.mutable.ArrayBuffer
import yuwakisa.fivel.models.basic.Shuffle

import scala.util.Random

class ShuffleSpec :

  val seed = 1234

  @Test def swap(): Unit =
    val deck = new Shuffle.Deck(5, seed)
    deck.swap(0)
    deck.swap(2)
    val expected = ArrayBuffer(3, 1, 4, 0, 2)
    assertEquals(expected, deck.deck)

  @Test def swapAll(): Unit =
    val deck = new Shuffle.Deck(5, seed)
    deck.swap(0)
    assertEquals(ArrayBuffer(3, 1, 2, 0, 4), deck.deck)
    deck.swap(1)
    assertEquals(ArrayBuffer(3, 2, 1, 0, 4), deck.deck)
    deck.swap(2)
    assertEquals(ArrayBuffer(3, 2, 4, 0, 1), deck.deck)
    deck.swap(3)
    assertEquals(ArrayBuffer(3, 2, 4, 0, 1), deck.deck)
    deck.swap(4)
    assertEquals(ArrayBuffer(3, 2, 4, 0, 1), deck.deck)

  @Test def pull0(): Unit =
    val shuffle = new Shuffle(5, seed)
    val actual = shuffle.draw
    val expected = 3
    assertEquals(expected, actual)

  @Test def pullSome(): Unit =
    val shuffle = new Shuffle(5, seed)
    val actual = (0 until 10).map({ i => shuffle.draw })
    assertEquals(Seq(3, 2, 4, 0, 1, 3, 1, 2, 0, 4), actual)
