package yuwakisa.fivel.models.tables

import yuwakisa.fivel.models.tables.Parse.State
import yuwakisa.fivel.models.tables._
import org.junit.Assert.{assertEquals, assertNull, assertTrue}
import org.junit.Test

import java.io._
import scala.collection.mutable
import scala.util.Random

class TableSpec :

  implicit val random: Random = Random(1234)

  def given_input(s: String): Parse =
    val r = BufferedReader(StringReader(s))
    Parse(r).go

  @Test def noPick(): Unit =
    val p = given_input("= no-Pick")
    val expected = ""
    val actual = p.tables.head.roll.mkString(",")
    assertEquals(expected, actual)

  @Test def onePick(): Unit =
    val p = given_input("= one-Pick\n-1 o,ne")
    val expected = "o,ne"
    val actual = p.tables.head.roll.mkString(",")
    assertEquals(expected, actual)

  @Test def twoPick(): Unit =
    val p = given_input("= two-Pick\n-1 o,ne\n-1 t'wo")
    val expected = "t'wo"
    val actual = p.tables.head.roll.mkString(",")
    assertEquals(expected, actual)

  @Test def nestedOnePick(): Unit =
    val p = given_input("= nested-One-Pick\n-1 o,ne\n--1 t'wo\n--1 t*hree")
    val expected = "o,ne,t*hree"
    val actual = p.tables.head.roll.mkString(",")
    assertEquals(expected, actual)
