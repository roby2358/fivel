package yuwakisa.servl

import org.junit.Assert.{assertEquals, assertTrue}
import org.junit.Test

import scala.util.matching.Regex

class PathRegexSpec :

  val pathRegex = Regex("""(/zing/.*)?(/zot/.*)?""")

  @Test def emptyString: Unit =
    val m0 = pathRegex.findFirstMatchIn("")
    assertTrue(m0.isDefined)
    println(m0.get.group(0))
    println(m0.get.group(1))
    assertEquals(2, m0.get.groupCount)
    assertTrue(true)

  @Test def empty: Unit =
    val m0 = pathRegex.findFirstMatchIn("/zing/woo")
    assertTrue(m0.isDefined)
    println(m0.get.group(0))
    println(m0.get.group(1))
    assertEquals(2, m0.get.groupCount)
    assertEquals("/zing/woo", m0.get.group(1))
    assertTrue(true)