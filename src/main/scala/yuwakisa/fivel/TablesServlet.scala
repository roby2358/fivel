package yuwakisa.fivel

import yuwakisa.fivel.models.basic.Resource
import yuwakisa.fivel.models.tables.Tables
import yuwakisa.servel.Content

import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import scala.util.matching.Regex

object TablesServlet :
  lazy val TableSources: Seq[String] = Resource("/tables").paths
  lazy val AllTables: Tables = Tables.load(TableSources)
  lazy val AllTableNames: Seq[String] = AllTables.toSeq

class TablesServlet extends HttpServlet :

  import TablesServlet._

  val rollRegex: Regex = """/roll/(.*)""".r

  override protected def doGet(request: HttpServletRequest, response: HttpServletResponse):Unit =
    println(s"GET tables ${request.getRequestURI} ${request.getPathInfo}")
    Option(request.getPathInfo) match
      case None =>
        Content.okJson(response, Map("tables" -> AllTableNames))
      case _ =>
        val name = rollRegex.findFirstMatchIn(request.getPathInfo).map(_.group(1)).headOption
        val result: Map[String, String] = AllTables(name.get)(Tables.mkString) match
          case Left(error) =>
            Map("error" -> error)
          case Right(string) =>
            Map("rolled" -> string)
        Content.okJson(response, result)
