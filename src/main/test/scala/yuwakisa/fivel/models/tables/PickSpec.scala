package yuwakisa.fivel.models.tables

import yuwakisa.fivel.models.basic.Resource
import org.junit.Assert.{assertEquals, assertNull, assertTrue}
import org.junit.Test

import java.io._

class PickSpec :

  @Test def powers(): Unit =
    val p = Resource("/tables/comicbookpowers.txt").reader.map(Parse(_).go).get
    println(p.tables.head.roll.mkString(", "))

  @Test def grievances(): Unit =
    val p = Resource("/tables/medevial_grievance.txt").reader.map(Parse(_).go).get
    println(p.tables.head.roll.mkString(", "))
    println(p.tables.last.roll.mkString(", "))
